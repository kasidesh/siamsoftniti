﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Core.Entities;

namespace Core.ViewModels
{
    public class SubjectViewModel
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Code { get; set; }
        public int DepartmentId { get; set; }
        public int CourseSyllabusId { get; set; }
        public string UserId { get; set; }
        public bool IsDeleted { get; set; }
        public System.DateTime CreatedDate { get; set; }
        public System.DateTime ModifiedDate { get; set; }


        public Departments Department { get; set; }
        public CourseSyllabuses CourseSyllabus { get; set; }
    }

    public class SubjectLiteViewModel
    {
        public int SubjectId { get; set; }
        public string SubjectName { get; set; }
        public string SubjectCode { get; set; }
        public string DepartmentName { get; set; }
        public string CourseSyllabusName { get; set; }
    }
}
