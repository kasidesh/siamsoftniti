﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Core.ViewModels
{
    public class QuestionViewModel
    {
        public int Id { get; set; }
        public Nullable<int> Position { get; set; }
        public string Name { get; set; }
        public List<AnswerViewModel> Answers { get; set; }
    }

    public class AnswerViewModel
    {
        public int Id { get; set; }
        public Nullable<int> Position { get; set; }
        public string Name { get; set; }
        public decimal Point { get; set; }
    }

    public class SubmitAnswerViewModel
    {
        public int UserAssessmentId { get; set; }
        public IList<AnswerSelection> AnswerSelections { get; set; }
    }

    public class AnswerSelection
    {
        public int QuestionId { get; set; }
        public int AnswerId { get; set; }
    }
}
