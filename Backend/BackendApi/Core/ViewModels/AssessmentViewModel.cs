﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Core.ViewModels
{
    public class AssessmentViewModel
    {
        public int Id { get; set; }
        public Nullable<int> SubjectId { get; set; }
        public string SubjectName { get; set; }
        public string Name { get; set; }
        public int Duration { get; set; }
        public Nullable<int> AmountOfQuestion { get; set; }
        public string ExplainationText { get; set; }
        public Nullable<int> PassingScore { get; set; }
        public Nullable<int> FullScore { get; set; }
        public string UserId { get; set; }
        public bool IsDeleted { get; set; }
        public int SchoolYear { get; set; }
        public System.DateTime CreatedDate { get; set; }
        public System.DateTime ModifiedDate { get; set; }
    }
}
