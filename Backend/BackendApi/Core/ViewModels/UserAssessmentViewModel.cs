﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Core.ViewModels
{
    public class UserAssessmentViewModel
    {
        public int Id { get; set; }
        public int AssessmentId { get; set; }
        public string AssessmentName { get; set; }
        public int AssessmentDuration { get; set; }
        public string AssessmentExplainationText { get; set; }
        public int SchoolYear { get; set; }
        public int SubjectId { get; set; }
        public string SubjectName { get; set; }
        public string SubjectCode { get; set; }
        public string DepartmentName { get; set; }
        public string CourseSyllabusName { get; set; }
    }

    public class UserAssessmentReport
    {
        public int Id { get; set; }
        public string UserId { get; set; }
        public int AssessmentId { get; set; }
        public Nullable<decimal> Score { get; set; }
        public bool IsFinished { get; set; }
        public System.DateTime CreatedDate { get; set; }
        public System.DateTime ModifiedDate { get; set; }
        public string AssessmentName { get; set; }
        public string UserName { get; set; }
    }
}
