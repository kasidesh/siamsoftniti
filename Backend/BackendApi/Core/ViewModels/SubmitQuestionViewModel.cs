﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Core.ViewModels
{
    public class SubmitQuestionViewModel
    {
        public int AssessmentId { get; set; }
        public string UserId { get; set; }
        public List<SubmitDetail> SubmitDetails { get; set; }
    }
    public class SubmitDetail
    {
        public int QuestionId { get; set; }
        public int AnswerId { get; set; }
        public DateTime SelectedAnswerDate { get; set; }
    }

    public class QuestionItem
    {
        public int QuestionId { get; set; }
        public int AssessmentId { get; set; }
        public string QuestionTitle { get; set; }
        public int QuestionPosition { get; set; }
        public List<AnswerItem> Answers { get; set; }
    }

    public class AnswerItem
    {
        public int Position { get; set; }
        public string Answer { get; set; }
        public decimal Point { get; set; }
    }
}
