﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DapperExtensions;
using System.Configuration;
using Core.Entities;
using System.Data.SqlClient;
using Core.ViewModels;
using System.Data;
using Dapper;

namespace Core.Repositories
{
    public class SubjectsRepository : IDisposable
    {
        private string _connectionString;
        public SubjectsRepository()
        {
            _connectionString = ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString;
        }

        public SubjectViewModel GetByIdExtend(int id)
        {
            //var lookup = new Dictionary<int, SubjectViewModel>();
            using (IDbConnection connection = new SqlConnection(_connectionString))
            {
                connection.Open();
                const string query =
@"select s.*, d.*, c.* from subjects s 
inner join Departments d on d.id = s.DepartmentId
inner join CourseSyllabuses c on c.id = s.CourseSyllabusId
where s.id = @Id
";
                var result = connection.Query<SubjectViewModel, Departments, CourseSyllabuses, SubjectViewModel>(query,
                    (s, d, c) =>
                    {
                        SubjectViewModel item;
                        //if (!lookup.TryGetValue(s.Id, out item))
                        //{
                        //    lookup.Add(s.Id, item = s);
                        //}
                        item = s;
                        if (item.Department == null)
                            item.Department = new Departments();
                        item.Department = d;
                        if (item.CourseSyllabus == null)
                            item.CourseSyllabus = new CourseSyllabuses();
                        item.CourseSyllabus = c;
                        return item;
                    }, new { Id = id });

                return result.FirstOrDefault();
            }
        }

        public Subjects GetById(int id)
        {
            using (SqlConnection cn = new SqlConnection(_connectionString))
            {
                cn.Open();
                Subjects updateItem = cn.Get<Subjects>(id);
                return updateItem;
            }
        }

        public IEnumerable<SubjectLiteViewModel> GetAllLite()
        {
            using (IDbConnection connection = new SqlConnection(_connectionString))
            {
                connection.Open();
                const string query =
@"select 
s.id as SubjectId,
s.name as SubjectName,
s.code as SubjectCode,
d.name as DepartmentName,
cs.name as CourseSyllabusName
 from subjects s 
inner join departments d on d.id = s.DepartmentId
inner join CourseSyllabuses cs on cs.id = s.CourseSyllabusId
where s.IsDeleted = 0
";
                var result = connection.Query<SubjectLiteViewModel>(query);
                return result;
            }
        }

        public IEnumerable<Subjects> GetAll()
        {
            using (SqlConnection cn = new SqlConnection(_connectionString))
            {
                cn.Open();
                var predicate = Predicates.Field<Subjects>(f => f.IsDeleted, Operator.Eq, false);
                return cn.GetList<Subjects>(predicate);
            }
        }

        public bool Add(Subjects item)
        {
            using (SqlConnection cn = new SqlConnection(_connectionString))
            {
                cn.Open();
                item.IsDeleted = false;
                item.CreatedDate = DateTime.Now;
                item.ModifiedDate = DateTime.Now;
                int id = cn.Insert(item);
                cn.Close();
                return true;
            }
        }

        public bool Update(Subjects item)
        {
            using (SqlConnection cn = new SqlConnection(_connectionString))
            {
                cn.Open();
                int id = item.Id;
                Subjects updateItem = cn.Get<Subjects>(id);
                updateItem.Code = item.Code;
                updateItem.DepartmentId = item.DepartmentId;
                updateItem.CourseSyllabusId = item.CourseSyllabusId;
                updateItem.UserId = item.UserId;
                updateItem.Name = item.Name;
                updateItem.IsDeleted = item.IsDeleted;
                updateItem.ModifiedDate = DateTime.Now;
                cn.Update(updateItem);
                cn.Close();
                return true;
            }
        }

        public bool Remove(int id)
        {
            using (SqlConnection cn = new SqlConnection(_connectionString))
            {
                cn.Open();
                Subjects deleteItem = cn.Get<Subjects>(id);
                deleteItem.IsDeleted = true;
                deleteItem.ModifiedDate = DateTime.Now;
                cn.Update(deleteItem);
                cn.Close();
                return true;
            }
        }

        public void Dispose()
        {

        }
    }
}
