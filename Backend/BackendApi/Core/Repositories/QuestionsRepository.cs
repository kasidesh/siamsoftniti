﻿using Core.Entities;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DapperExtensions;

namespace Core.Repositories
{
    public class QuestionssRepository
    {
        private string _connectionString;
        public QuestionssRepository()
        {
            _connectionString = ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString;
        }

        public Questions GetById(int id)
        {
            using (SqlConnection cn = new SqlConnection(_connectionString))
            {
                cn.Open();
                Questions updateItem = cn.Get<Questions>(id);
                return updateItem;
            }
        }

        public IEnumerable<Questions> GetAll()
        {
            using (SqlConnection cn = new SqlConnection(_connectionString))
            {
                cn.Open();
                var predicate = Predicates.Field<Questions>(f => f.IsDeleted, Operator.Eq, false);
                return cn.GetList<Questions>(predicate);
            }
        }

        public IEnumerable<Questions> GetAllByAssessmentId(int assessmentId)
        {
            using (SqlConnection cn = new SqlConnection(_connectionString))
            {
                cn.Open();
                var predicate = Predicates.Field<Questions>(f => f.AssessmentId, Operator.Eq, assessmentId);
                return cn.GetList<Questions>(predicate).Where(f => f.IsDeleted == false);
            }
        }

        public int Add(Questions item)
        {
            using (SqlConnection cn = new SqlConnection(_connectionString))
            {
                cn.Open();
                item.IsDeleted = false;
                item.CreatedDate = DateTime.Now;
                item.ModifiedDate = DateTime.Now;
                int id = cn.Insert(item);
                cn.Close();
                return id;
            }
        }

        public bool Update(Questions item)
        {
            using (SqlConnection cn = new SqlConnection(_connectionString))
            {
                cn.Open();
                int id = item.Id;
                Questions updateItem = cn.Get<Questions>(id);
                updateItem.Name = item.Name;
                updateItem.Position = item.Position;
                updateItem.IsDeleted = item.IsDeleted;
                updateItem.ModifiedDate = DateTime.Now;
                cn.Update(updateItem);
                cn.Close();
                return true;
            }
        }

        public bool Remove(int id)
        {
            using (SqlConnection cn = new SqlConnection(_connectionString))
            {
                cn.Open();
                Questions deleteItem = cn.Get<Questions>(id);
                deleteItem.IsDeleted = true;
                deleteItem.ModifiedDate = DateTime.Now;
                cn.Update(deleteItem);
                cn.Close();
                return true;
            }
        }

        public void Dispose()
        {

        }
    }
}
