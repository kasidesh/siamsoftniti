﻿using Core.Entities;
using Core.ViewModels;
using DapperExtensions;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Dapper;
using System.Data;

namespace Core.Repositories
{
    public class UserAssessmentsRepository
    {
        private string _connectionString;
        public UserAssessmentsRepository()
        {
            _connectionString = ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString;
        }

        public UserAssessments GetById(int id)
        {
            using (SqlConnection cn = new SqlConnection(_connectionString))
            {
                cn.Open();
                UserAssessments updateItem = cn.Get<UserAssessments>(id);
                return updateItem;
            }
        }

        public IEnumerable<UserAssessmentReport> GetUserAssessmentByAssessmentId(int id)
        {
            using (IDbConnection connection = new SqlConnection(_connectionString))
            {
                connection.Open();
                const string query =
@"select ua.*, a.name as AssessmentName, u.username as UserName from userAssessments ua
inner join assessments a on a.id = ua.AssessmentId
inner join AspNetUsers u on u.id = ua.userid
where ua.AssessmentId = @Id
order by u.username";
                var result = connection.Query<UserAssessmentReport>(query, new { Id = id });
                return result;
            }
        }

        public IEnumerable<UserAssessmentViewModel> GetActiveUserAssessment(string userId)
        {
            using (IDbConnection connection = new SqlConnection(_connectionString))
            {
                connection.Open();
                const string query =
@"select 
ua.id as Id,
a.id as AssessmentId,
a.schoolyear as SchoolYear,
a.name as AssessmentName,
a.Duration as AssessmentDuration,
a.ExplainationText as AssessmentExplainationText,
s.id as SubjectId,
s.name as SubjectName,
s.code as SubjectCode,
d.name as DepartmentName,
cs.name as CourseSyllabusName
 from userassessments as ua
inner join assessments as a on a.id = ua.assessmentId
inner join subjects as s on s.id = a.subjectid
inner join departments as d on d.id = s.departmentId
inner join courseSyllabuses as cs on cs.id = s.CourseSyllabusId
where ua.UserId = @userId
";
                var result = connection.Query<UserAssessmentViewModel>(query, new { userId = userId });
                return result;
            }
        }

        public IEnumerable<UserAssessments> GetActive(string userId)
        {
            using (SqlConnection cn = new SqlConnection(_connectionString))
            {
                cn.Open();
                var pg = new PredicateGroup { Operator = GroupOperator.And, Predicates = new List<IPredicate>() };
                pg.Predicates.Add(Predicates.Field<UserAssessments>(f => f.IsFinished, Operator.Eq, false));
                pg.Predicates.Add(Predicates.Field<UserAssessments>(f => f.UserId, Operator.Eq, userId));
                return cn.GetList<UserAssessments>(pg);
            }
        }

        public bool Add(UserAssessments item)
        {
            using (SqlConnection cn = new SqlConnection(_connectionString))
            {
                cn.Open();
                item.CreatedDate = DateTime.Now;
                item.ModifiedDate = DateTime.Now;
                int id = cn.Insert(item);
                cn.Close();
                return true;
            }
        }

        public bool Update(UserAssessments item)
        {
            using (SqlConnection cn = new SqlConnection(_connectionString))
            {
                cn.Open();
                int id = item.Id;
                UserAssessments updateItem = cn.Get<UserAssessments>(id);
                updateItem.ModifiedDate = DateTime.Now;
                updateItem.Score = item.Score;
                updateItem.IsFinished = item.IsFinished;
                cn.Update(updateItem);
                cn.Close();
                return true;
            }
        }

        public bool Finalize(int id)
        {
            using (SqlConnection cn = new SqlConnection(_connectionString))
            {
                cn.Open();
                UserAssessments deleteItem = cn.Get<UserAssessments>(id);
                deleteItem.IsFinished = true;
                deleteItem.ModifiedDate = DateTime.Now;
                cn.Update(deleteItem);
                cn.Close();
                return true;
            }
        }

        public void Dispose()
        {

        }
    }
}
