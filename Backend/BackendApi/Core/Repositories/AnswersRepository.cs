﻿using Core.Entities;
using DapperExtensions;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Core.Repositories
{
    public class AnswersRepository
    {
        private string _connectionString;
        public AnswersRepository()
        {
            _connectionString = ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString;
        }

        public Answers GetById(int id)
        {
            using (SqlConnection cn = new SqlConnection(_connectionString))
            {
                cn.Open();
                Answers updateItem = cn.Get<Answers>(id);
                return updateItem;
            }
        }

        public IEnumerable<Answers> GetAll()
        {
            using (SqlConnection cn = new SqlConnection(_connectionString))
            {
                cn.Open();
                var predicate = Predicates.Field<Answers>(f => f.IsDeleted, Operator.Eq, false);
                return cn.GetList<Answers>(predicate);
            }
        }

        public IEnumerable<Answers> GetAll(int questionId)
        {
            using (SqlConnection cn = new SqlConnection(_connectionString))
            {
                cn.Open();
                var predicate = Predicates.Field<Answers>(f => f.QuestionId, Operator.Eq, questionId);
                return cn.GetList<Answers>(predicate).Where(f => f.IsDeleted == false);
            }
        }

        public bool Add(Answers item)
        {
            using (SqlConnection cn = new SqlConnection(_connectionString))
            {
                cn.Open();
                item.IsDeleted = false;
                item.CreatedDate = DateTime.Now;
                item.ModifiedDate = DateTime.Now;
                int id = cn.Insert(item);
                cn.Close();
                return true;
            }
        }

        public bool Update(Answers item)
        {
            using (SqlConnection cn = new SqlConnection(_connectionString))
            {
                cn.Open();
                int id = item.Id;
                Answers updateItem = cn.Get<Answers>(id);
                updateItem.Name = item.Name;

                updateItem.IsDeleted = item.IsDeleted;
                updateItem.ModifiedDate = DateTime.Now;
                cn.Update(updateItem);
                cn.Close();
                return true;
            }
        }

        public bool Remove(int id)
        {
            using (SqlConnection cn = new SqlConnection(_connectionString))
            {
                cn.Open();
                Answers deleteItem = cn.Get<Answers>(id);
                deleteItem.IsDeleted = true;
                deleteItem.ModifiedDate = DateTime.Now;
                cn.Update(deleteItem);
                cn.Close();
                return true;
            }
        }

        public bool PermanentRemove(int id)
        {
            using (SqlConnection cn = new SqlConnection(_connectionString))
            {
                cn.Open();
                Answers answer = cn.Get<Answers>(id);
                cn.Delete<Answers>(answer);
                cn.Close();
                return true;
            }
        }

        public void Dispose()
        {

        }
    }
}
