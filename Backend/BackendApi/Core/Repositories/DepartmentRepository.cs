﻿using Core.Entities;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DapperExtensions;
using System.Configuration;

namespace Core.Repositories
{
    public class DepartmentsRepository : IDisposable
    {
        private string _connectionString;
        public DepartmentsRepository()
        {
            _connectionString = ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString;
        }

        public Departments GetById(int id)
        {
            using (SqlConnection cn = new SqlConnection(_connectionString))
            {
                cn.Open();
                Departments updateItem = cn.Get<Departments>(id);
                return updateItem;
            }
        }

        public IEnumerable<Departments> GetAll()
        {
            using (SqlConnection cn = new SqlConnection(_connectionString))
            {
                cn.Open();
                var predicate = Predicates.Field<Departments>(f => f.IsDeleted, Operator.Eq, false);
                return cn.GetList<Departments>(predicate);
            }
        }

        public bool Add(Departments item)
        {
            using (SqlConnection cn = new SqlConnection(_connectionString))
            {
                cn.Open();
                item.IsDeleted = false;
                item.CreatedDate = DateTime.Now;
                item.ModifiedDate = DateTime.Now;
                int id = cn.Insert(item);
                cn.Close();
                return true;
            }
        }

        public bool Update(Departments item)
        {
            using (SqlConnection cn = new SqlConnection(_connectionString))
            {
                cn.Open();
                int id = item.Id;
                Departments updateItem = cn.Get<Departments>(id);
                updateItem.Name = item.Name;
                updateItem.IsDeleted = item.IsDeleted;
                updateItem.ModifiedDate = DateTime.Now;
                cn.Update(updateItem);
                cn.Close();
                return true;
            }
        }

        public bool Remove(int id)
        {
            using (SqlConnection cn = new SqlConnection(_connectionString))
            {
                cn.Open();
                Departments deleteItem = cn.Get<Departments>(id);
                deleteItem.IsDeleted = true;
                deleteItem.ModifiedDate = DateTime.Now;
                cn.Update(deleteItem);
                cn.Close();
                return true;
            }
        }

        public void Dispose()
        {
            
        }
    }
}
