﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DapperExtensions;
using System.Configuration;
using Core.Entities;
using System.Data.SqlClient;
using Core.ViewModels;
using System.Data;
using Dapper;

namespace Core.Repositories
{
    public class AssessmentsRepository : IDisposable
    {
        private string _connectionString;
        public AssessmentsRepository()
        {
            _connectionString = ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString;
        }

        public AssessmentViewModel GetByIdLite(int id)
        {
            using (IDbConnection connection = new SqlConnection(_connectionString))
            {
                connection.Open();
                const string query =
@"select a.*, s.id as SubjectId , s.name as SubjectName from assessments a
inner join Subjects s on s.id = a.SubjectId
where a.IsDeleted = 0 and a.id = @Id
";
                var result = connection.Query<AssessmentViewModel>(query, new { Id = id });
                return result.FirstOrDefault();
            }
        }

        public Assessments GetById(int id)
        {
            using (SqlConnection cn = new SqlConnection(_connectionString))
            {
                cn.Open();
                Assessments updateItem = cn.Get<Assessments>(id);
                return updateItem;
            }
        }

        public IEnumerable<AssessmentViewModel> GetAllLite()
        {
            using (IDbConnection connection = new SqlConnection(_connectionString))
            {
                connection.Open();
                const string query =
@"select a.*, s.name as SubjectName from assessments a
inner join Subjects s on s.id = a.SubjectId
where a.IsDeleted = 0
";
                var result = connection.Query<AssessmentViewModel>(query);
                return result;
            }
        }

        public IEnumerable<Assessments> GetAll()
        {
            using (SqlConnection cn = new SqlConnection(_connectionString))
            {
                cn.Open();
                var predicate = Predicates.Field<Assessments>(f => f.IsDeleted, Operator.Eq, false);
                return cn.GetList<Assessments>(predicate);
            }
        }

        public bool Add(Assessments item)
        {
            using (SqlConnection cn = new SqlConnection(_connectionString))
            {
                cn.Open();
                item.IsDeleted = false;
                item.CreatedDate = DateTime.Now;
                item.ModifiedDate = DateTime.Now;
                int id = cn.Insert(item);
                cn.Close();
                return true;
            }
        }

        public bool Update(Assessments item)
        {
            using (SqlConnection cn = new SqlConnection(_connectionString))
            {
                cn.Open();
                int id = item.Id;
                Assessments updateItem = cn.Get<Assessments>(id);
                updateItem.Name = item.Name;
                updateItem.AmountOfQuestion = item.AmountOfQuestion;
                updateItem.Duration = item.Duration;
                updateItem.ExplainationText = item.ExplainationText;
                updateItem.FullScore = item.FullScore;
                updateItem.PassingScore = item.PassingScore;
                updateItem.SubjectId = item.SubjectId;
                updateItem.UserId = item.UserId;
                updateItem.SchoolYear = item.SchoolYear;
                updateItem.IsDeleted = item.IsDeleted;
                updateItem.ModifiedDate = DateTime.Now;
                cn.Update(updateItem);
                cn.Close();
                return true;
            }
        }

        public bool Remove(int id)
        {
            using (SqlConnection cn = new SqlConnection(_connectionString))
            {
                cn.Open();
                Assessments deleteItem = cn.Get<Assessments>(id);
                deleteItem.IsDeleted = true;
                deleteItem.ModifiedDate = DateTime.Now;
                cn.Update(deleteItem);
                cn.Close();
                return true;
            }
        }

        public void Dispose()
        {

        }
    }
}
