﻿using Core.Entities;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DapperExtensions;

namespace Core.Repositories
{
    public class UserAssessmentDetailsRepository
    {
        private string _connectionString;
        public UserAssessmentDetailsRepository()
        {
            _connectionString = ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString;
        }

        public UserAssessmentDetails GetById(int id)
        {
            using (SqlConnection cn = new SqlConnection(_connectionString))
            {
                cn.Open();
                UserAssessmentDetails updateItem = cn.Get<UserAssessmentDetails>(id);
                return updateItem;
            }
        }

        public bool Add(UserAssessmentDetails item)
        {
            using (SqlConnection cn = new SqlConnection(_connectionString))
            {
                cn.Open();
                item.CreatedDate = DateTime.Now;
                item.ModifiedDate = DateTime.Now;
                int id = cn.Insert(item);
                cn.Close();
                return true;
            }
        }

        public bool Update(UserAssessmentDetails item)
        {
            using (SqlConnection cn = new SqlConnection(_connectionString))
            {
                cn.Open();
                int id = item.Id;
                UserAssessmentDetails updateItem = cn.Get<UserAssessmentDetails>(id);
                updateItem.ModifiedDate = DateTime.Now;
                cn.Update(updateItem);
                cn.Close();
                return true;
            }
        }

        public decimal FinalizeScore(int userAssessmentId)
        {
            using (SqlConnection cn = new SqlConnection(_connectionString))
            {
                cn.Open();
                var predicate = Predicates.Field<UserAssessmentDetails>(f => f.UserAssessmentId, Operator.Eq, userAssessmentId);
                return cn.GetList<UserAssessmentDetails>(predicate).Sum(n => n.Point);
            }
        }

        public void Dispose()
        {

        }
    }
}
