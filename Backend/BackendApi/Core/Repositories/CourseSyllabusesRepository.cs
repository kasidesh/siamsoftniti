﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DapperExtensions;
using System.Configuration;
using Core.Entities;
using System.Data.SqlClient;

namespace Core.Repositories
{
    public class CourseSyllabusesRepository : IDisposable
    {
        private string _connectionString;
        public CourseSyllabusesRepository()
        {
            _connectionString = ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString;
        }

        public CourseSyllabuses GetById(int id)
        {
            using (SqlConnection cn = new SqlConnection(_connectionString))
            {
                cn.Open();
                CourseSyllabuses updateItem = cn.Get<CourseSyllabuses>(id);
                return updateItem;
            }
        }

        public IEnumerable<CourseSyllabuses> GetAll()
        {
            using (SqlConnection cn = new SqlConnection(_connectionString))
            {
                cn.Open();
                var predicate = Predicates.Field<CourseSyllabuses>(f => f.IsDeleted, Operator.Eq, false);
                return cn.GetList<CourseSyllabuses>(predicate);
            }
        }

        public bool Add(CourseSyllabuses item)
        {
            using (SqlConnection cn = new SqlConnection(_connectionString))
            {
                cn.Open();
                item.IsDeleted = false;
                item.CreatedDate = DateTime.Now;
                item.ModifiedDate = DateTime.Now;
                int id = cn.Insert(item);
                cn.Close();
                return true;
            }
        }

        public bool Update(CourseSyllabuses item)
        {
            using (SqlConnection cn = new SqlConnection(_connectionString))
            {
                cn.Open();
                int id = item.Id;
                CourseSyllabuses updateItem = cn.Get<CourseSyllabuses>(id);
                updateItem.Name = item.Name;
                updateItem.IsDeleted = item.IsDeleted;
                updateItem.ModifiedDate = DateTime.Now;
                cn.Update(updateItem);
                cn.Close();
                return true;
            }
        }

        public bool Remove(int id)
        {
            using (SqlConnection cn = new SqlConnection(_connectionString))
            {
                cn.Open();
                CourseSyllabuses deleteItem = cn.Get<CourseSyllabuses>(id);
                deleteItem.IsDeleted = true;
                deleteItem.ModifiedDate = DateTime.Now;
                cn.Update(deleteItem);
                cn.Close();
                return true;
            }
        }

        public void Dispose()
        {

        }
    }
}
