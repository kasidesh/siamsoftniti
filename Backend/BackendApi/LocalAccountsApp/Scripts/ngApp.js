﻿var chulaApp = angular.module('Chula', [ 'ngAnimate', 'toaster']);

chulaApp.controller('MainController', function ($scope, $http, toaster) {
    

    $scope.editMode = false;
    $scope.questionTitle = '';
    $scope.questionPosition = 0;

    $scope.answers = [
        { Answer: '', Point: 1, Position: 1 },
        { Answer: '', Point: 0, Position: 2 },
        { Answer: '', Point: 0, Position: 3 },
        { Answer: '', Point: 0, Position: 4 }
    ];

    $scope.CreateNewAnswer = function () {
        $scope.questionId = null;
        $scope.editMode = false;
        $scope.questionTitle = '';
        $scope.questionPosition = '';
        $scope.answers = [
           { Answer: '', Point: 1, Position: 1 },
           { Answer: '', Point: 0, Position: 2 },
           { Answer: '', Point: 0, Position: 3 },
           { Answer: '', Point: 0, Position: 4 }
        ];
    };

    $scope.removeAnswer = function (ans) {
        $scope.answers.splice($scope.answers.indexOf(ans), 1);
    };

    $scope.addNewAnswer = function () {
        $scope.answers.push({ Answer: '', Point: 0, Position: 0 });
    };

    $scope.LoadAnswer = function (questionId) {
        $scope.editMode = true;
        $scope.questionId = questionId;
        var token = sessionStorage.getItem('accessToken');
        var appHeaders = {};
        if (token) {
            appHeaders.Authorization = 'Bearer ' + token;
        }

        $http({
            method: 'GET',
            url: '/api/answer/loadanswer/' + questionId,
            headers: appHeaders
        }).success(function (data) {
            $scope.answers = [];
            $scope.assId = data.AssessmentId;
            $scope.answers.push.apply($scope.answers, data.Answers);
            $scope.questionTitle = data.QuestionTitle;
            $scope.questionPosition = data.QuestionPosition;
            toaster.pop('success', 'load answer', 'some message');
        }).error(function (err) {
            toaster.pop('error', 'load answer', err);
            $scope.editMode = false;
        });
    };

    $scope.addQuestion = function () {
        console.log($scope.assId);

        var item = {
            AssessmentId: $scope.assId,
            QuestionTitle: $scope.questionTitle,
            QuestionPosition: $scope.questionPosition,
            Answers: $scope.answers
        }
        var token = sessionStorage.getItem('accessToken');
        var appHeaders = {};
        if (token) {
            appHeaders.Authorization = 'Bearer ' + token;
        }

        console.log(item);

        $http({
            method: 'POST',
            url: '/api/answer/addQuestion',
            data: item,
            headers: appHeaders
        }).success(function (data) {
            $scope.questionTitle = '';
            $scope.questionPosition = '';
            $scope.answers = [
                { Answer: '', Point: 1, Position: 1 },
                { Answer: '', Point: 0, Position: 2 },
                { Answer: '', Point: 0, Position: 3 },
                { Answer: '', Point: 0, Position: 4 }
            ];
            toaster.pop('success', 'add question', 'some message');
        }).error(function (err) {
            toaster.pop('error', 'add question', err);
        });
    };

    $scope.updateQuestion = function () {
        console.log($scope.assId);

        var item = {
            QuestionId: $scope.questionId,
            AssessmentId: $scope.assId,
            QuestionTitle: $scope.questionTitle,
            QuestionPosition: $scope.questionPosition,
            Answers: $scope.answers
        }
        var token = sessionStorage.getItem('accessToken');
        var appHeaders = {};
        if (token) {
            appHeaders.Authorization = 'Bearer ' + token;
        }

        console.log(item);

        $http({
            method: 'POST',
            url: '/api/answer/updateQuestion',
            data: item,
            headers: appHeaders
        }).success(function (data) {
            $scope.questionTitle = '';
            $scope.questionPosition = '';
            $scope.answers = [
                { Answer: '', Point: 1, Position: 1 },
                { Answer: '', Point: 0, Position: 2 },
                { Answer: '', Point: 0, Position: 3 },
                { Answer: '', Point: 0, Position: 4 }
            ];
            $scope.editMode = false;
            toaster.pop('success', 'update question', 'some message');
        }).error(function (err) {
            toaster.pop('error', 'update question', err);
        });
    };
});
