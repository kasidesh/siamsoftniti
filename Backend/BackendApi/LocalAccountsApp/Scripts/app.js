﻿function AnswerViewModel(answer, point) {
    var self = this;
    self.answer = ko.observable(answer);
    self.point = ko.observable(point);
}

function ViewModel() {
    var self = this;

    var tokenKey = 'accessToken';

    self.answers = ko.observableArray([
        new AnswerViewModel("Red", 1),
        new AnswerViewModel("White", 2),
        new AnswerViewModel("Black", 0),
        new AnswerViewModel("Green", 0),
    ]);

    self.removeAnswer = function (ans) { self.answers.remove(ans) };
    self.addAnswer = function () {
        self.answers.push(new AnswerViewModel("", 0));
    }

    self.result = ko.observable();
    self.user = ko.observable();

    self.registerEmail = ko.observable();
    self.registerPassword = ko.observable();
    self.registerPassword2 = ko.observable();
    self.registerRoleName = ko.observable();

    self.loginEmail = ko.observable();
    self.loginPassword = ko.observable();

    function showError(jqXHR) {
        self.result(jqXHR.status + ': ' + jqXHR.statusText);
    }

    self.callApi = function () {
        self.result('');

        var token = sessionStorage.getItem(tokenKey);
        var headers = {};
        if (token) {
            headers.Authorization = 'Bearer ' + token;
        }

        $.ajax({
            type: 'GET',
            url: '/api/values',
            headers: headers
        }).done(function (data) {
            self.result(data);
        }).fail(showError);
    }

    self.callValueApi = function () {
        self.result('');

        var token = sessionStorage.getItem(tokenKey);
        var headers = {};
        if (token) {
            headers.Authorization = 'Bearer ' + token;
        }

        $.ajax({
            type: 'GET',
            url: '/api/Values',
            headers: headers
        }).done(function (data) {
            self.result(data);
        }).fail(showError);
    }

    self.callActiveAssessmentApi = function () {
        self.result('');

        var token = sessionStorage.getItem(tokenKey);
        var headers = {};
        if (token) {
            headers.Authorization = 'Bearer ' + token;
        }

        $.ajax({
            type: 'GET',
            url: '/api/Assessment/GetActiveAssessments',
            headers: headers
        }).done(function (data) {
            self.result(data);
        }).fail(showError);
    }

    self.callQuestionApi = function () {
        self.result('');

        var token = sessionStorage.getItem(tokenKey);
        var headers = {};
        if (token) {
            headers.Authorization = 'Bearer ' + token;
        }

        $.ajax({
            type: 'GET',
            url: '/api/Assessment/3/Questions',
            headers: headers
        }).done(function (data) {
            self.result(data);
        }).fail(showError);
    }

    self.register = function () {
        console.log('register');
        self.result('');

        var data = {
            Email: self.registerEmail(),
            Password: self.registerPassword(),
            ConfirmPassword: self.registerPassword2(),
            RoleName: $("#ddlRole").val()
            //RoleName: self.registerRoleName()
        };

        $.ajax({
            type: 'POST',
            url: '/api/Account/Register',
            contentType: 'application/json; charset=utf-8',
            data: JSON.stringify(data)
        }).done(function (r) {
            
            self.result("Done!");
            window.location = '/Users/' + data.RoleName;
        }).fail(showError);
    }

    self.login = function () {
        self.result('');

        var loginData = {
            grant_type: 'password',
            username: self.loginEmail(),
            password: self.loginPassword()
        };

        $.ajax({
            type: 'POST',
            url: '/Token',
            data: loginData
        }).done(function (data) {
            console.log('login success');
            self.user(data.userName);
            // Cache the access token in session storage.
            sessionStorage.setItem(tokenKey, data.access_token);
            window.location = '/Dashboards/Dashboard_1';
        }).fail(showError);
    }

    self.logout = function () {
        var token = sessionStorage.getItem(tokenKey);
        var headers = {};
        if (token) {
            headers.Authorization = 'Bearer ' + token;
        }

        console.log('press logout');

        $.ajax({
            type: 'POST',
            url: '/api/Account/Logout',
            headers: headers
        }).done(function (data) {
            console.log('logout success');
            self.user('');
            sessionStorage.removeItem(tokenKey)

            window.location = '/Pages/Login';
        }).fail(function (error) {
            //worst case senario
            console.log('logout error');
            console.log(error);

            self.user('');
            sessionStorage.removeItem(tokenKey)
            window.location = '/Pages/Login';
        });
    }
}

var app = new ViewModel();
ko.applyBindings(app);