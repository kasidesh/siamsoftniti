﻿using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Web;
using System.Threading.Tasks;


namespace AssessmentApp.Models
{
    public class MyUser : IdentityUser
    {
        public async Task<ClaimsIdentity> GenerateUserIdentityAsync(UserManager<MyUser> manager, string authenticationType)
        {
            // Note the authenticationType must match the one defined in CookieAuthenticationOptions.AuthenticationType
            var userIdentity = await manager.CreateIdentityAsync(this, authenticationType);
            // Add custom user claims here
            return userIdentity;
        }

        public virtual MyUserInfo MyUserInfos { get; set; }
        public class MyUserInfo
        {
            public int Id { get; set; }
            public string FirstName { get; set; }
            public string LastName { get; set; }
        }

        public class MyDbContext : IdentityDbContext<MyUser>
        {
            public MyDbContext()
                : base("DefaultConnection")
            {
            }
            public System.Data.Entity.DbSet<MyUserInfo> MyUserInfo { get; set; }

            public static MyDbContext Create()
            {
                return new MyDbContext();
            }
        }
    }
}