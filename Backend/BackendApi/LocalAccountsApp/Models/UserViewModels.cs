﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AssessmentApp.Models
{
    public class UserViewModels
    {
        public string Email { get; set; }
        public string Password { get; set; }
        public string UserName { get; set; }
        public string RoleName { get; set; }
    }


    public class ImportUserViewModel
    {
        public string Username { get; set; }
        public string Password { get; set; }
    }
}