﻿using AssessmentApp.Models;
using Core.Entities;
using Core.Repositories;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using PagedList;

namespace AssessmentApp.Controllers
{
    [Authorize(Roles = "Admin, Instructor")]
    public class DepartmentsController : Controller
    {
        private DepartmentsRepository db = new DepartmentsRepository();

        // GET: /Departments/
         [Authorize(Roles = "Admin, Instructor")]
        public ActionResult Index(int? page)
        {
            int pageSize = 10;
            int pageNumber = (page ?? 1);
            var pageResult = db.GetAll().ToPagedList(pageNumber, pageSize);
            return View(pageResult);
        }

        // GET: /Departments/Details/5
         [Authorize(Roles = "Admin, Instructor")]
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Departments worker = db.GetById(id.Value);
            if (worker == null)
            {
                return HttpNotFound();
            }
            return View(worker);
        }

        // GET: /Departments/Create
         [Authorize(Roles = "Admin, Instructor")]
        public ActionResult Create()
        {
            return View();
        }

        // POST: /Departments/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        [Authorize(Roles = "Admin, Instructor")]
        public ActionResult Create([Bind(Include = "Id, Name, IsDeleted, CreatedDate, ModifiedDate")] Departments worker)
        {
            if (ModelState.IsValid)
            {
                db.Add(worker);
                return RedirectToAction("Index");
            }

            return View(worker);
        }

        // GET: /Departments/Edit/5
         [Authorize(Roles = "Admin, Instructor")]
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Departments worker = db.GetById(id.Value);
            if (worker == null)
            {
                return HttpNotFound();
            }
            return View(worker);
        }

        // POST: /Departments/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [Authorize(Roles = "Admin, Instructor")]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "Id, Name, IsDeleted, CreatedDate, ModifiedDate")] Departments worker)
        {
            if (ModelState.IsValid)
            {
                //db.Entry(worker).State = EntityState.Modified;
                //db.SaveChanges();
                db.Update(worker);
                return RedirectToAction("Index");
            }
            return View(worker);
        }

        // GET: /Departments/Delete/5
         [Authorize(Roles = "Admin, Instructor")]
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Departments worker = db.GetById(id.Value);
            if (worker == null)
            {
                return HttpNotFound();
            }
            return View(worker);
        }

        // POST: /Departments/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        [Authorize(Roles = "Admin, Instructor")]
        public ActionResult DeleteConfirmed(int id)
        {
            //Department worker = db.GetById(id);
            db.Remove(id);
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}