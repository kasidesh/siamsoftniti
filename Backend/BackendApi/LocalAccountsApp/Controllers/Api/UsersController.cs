﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace AssessmentApp.Controllers.Api
{
    /// <summary>
    /// Manage User - import instructor, import student, create an instructor, create a student
    /// </summary>
    /// 
    [Authorize]
    [RoutePrefix("api/Users")]
    public class UsersController : ApiController
    {
        [Route("UserInfo")]
        public string Get()
        {
            var userName = this.RequestContext.Principal.Identity.Name;
            return String.Format("Hello, {0}.", userName);
        }
    }
}
