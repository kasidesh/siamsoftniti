﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace AssessmentApp.Controllers
{
    [Authorize(Roles = "Admin")]
    public class ValuesController : ApiController
    {
        // GET api/values
        public string Get()
        {
            //make change for test something
            var userName = this.RequestContext.Principal.Identity.Name;
            return String.Format("Hello, {0}.", userName);
        }
    }
}
