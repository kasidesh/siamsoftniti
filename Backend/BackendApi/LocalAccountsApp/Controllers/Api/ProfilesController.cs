﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace AssessmentApp.Controllers.Api
{
    /// <summary>
    /// see user profile(all role)
    /// see activity logs(all role)
    /// see assessment results history(student)
    /// see course and assessment(instructor, admin)
    /// </summary>
    /// 
    [Authorize]
    public class ProfilesController : ApiController
    {
        // GET api/values
        public string Get()
        {
            var userName = this.RequestContext.Principal.Identity.Name;
            return String.Format("Hello, {0}.", userName);
        }
    }
}
