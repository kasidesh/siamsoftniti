﻿using Core.Repositories;
using Core.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace AssessmentApp.Controllers.Api
{
    [Authorize(Roles = "Admin, Instructor")]
    [RoutePrefix("api/Answer")]
    public class AnswerController : ApiController
    {
        private AssessmentsRepository _assessment = new AssessmentsRepository();
        private QuestionssRepository _question = new QuestionssRepository();
        private AnswersRepository _answer = new AnswersRepository();

        [Route("addQuestion")]
        [HttpPost]
        public IHttpActionResult addQuestion(QuestionItem saveItem)
        {
            if (saveItem != null)
            {
                if (_assessment.GetById(saveItem.AssessmentId) == null)
                    return BadRequest("There is no assessmentId in our system");

                int questionId = _question.Add(new Core.Entities.Questions
                {
                    AssessmentId = saveItem.AssessmentId,
                    Name = saveItem.QuestionTitle,
                    Position = saveItem.QuestionPosition,
                    Star = false
                });

                for (int i = 0; i < saveItem.Answers.Count; i++)
                {
                    _answer.Add(new Core.Entities.Answers
                    {
                        QuestionId = questionId,
                        Name = saveItem.Answers[i].Answer,
                        Point = saveItem.Answers[i].Point,
                        Position = i + 1
                    });
                }
                return Ok(_question.GetById(questionId));
            }

            return BadRequest("invalid json format for this URI");
        }


        [Route("updateQuestion")]
        [HttpPost]
        public IHttpActionResult updateQuestion(QuestionItem saveItem)
        {
            if (saveItem != null)
            {
                if (_assessment.GetById(saveItem.AssessmentId) == null)
                    return BadRequest("There is no assessmentId in our system");

                var question = _question.GetById(saveItem.QuestionId);
                question.Name = saveItem.QuestionTitle;
                question.Position = saveItem.QuestionPosition;
                _question.Update(question);

                //remove all existed answers
                var existedAnswers = _answer.GetAll(question.Id).ToList();
                for (int i = 0; i < existedAnswers.Count(); i++)
                {
                    _answer.PermanentRemove(existedAnswers[i].Id);
                }

                for (int i = 0; i < saveItem.Answers.Count; i++)
                {
                    _answer.Add(new Core.Entities.Answers
                    {
                        QuestionId = question.Id,
                        Name = saveItem.Answers[i].Answer,
                        Point = saveItem.Answers[i].Point,
                        Position = i + 1
                    });
                }
                return Ok(_question.GetById(question.Id));
            }

            return BadRequest("invalid json format for this URI");
        }

        [Route("LoadAnswer/{id}")]
        [HttpGet]
        public IHttpActionResult LoadAnswer(int id)
        {
            var question = _question.GetById(id);
            var answers = _answer.GetAll(id);

            QuestionItem result = new QuestionItem();
            result.AssessmentId = question.AssessmentId;
            result.QuestionTitle = question.Name;
            result.QuestionPosition = question.Position ?? 0;
            result.Answers = answers.Select(n => new AnswerItem
            {
                Point = n.Point,
                Answer = n.Name,
                Position = n.Position ?? 0
            }).ToList();
            return Ok(result);
        }
    }
}
