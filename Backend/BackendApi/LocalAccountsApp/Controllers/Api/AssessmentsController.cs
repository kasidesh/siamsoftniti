﻿using Core.Entities;
using Core.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using Microsoft.AspNet.Identity;
using Core.ViewModels;
using AssessmentApp.Helpers;

namespace AssessmentApp.Controllers.Api
{
    /// <summary>
    /// Manage assessment : add, edit, delete assessment
    /// </summary>
    /// 
    [Authorize(Roles = "Student")]
    [RoutePrefix("api/Assessment")]
    public class AssessmentsController : ApiController
    {
        private UserAssessmentDetailsRepository _userAssessmentDetail = new UserAssessmentDetailsRepository();
        private UserAssessmentsRepository _userAssessment = new UserAssessmentsRepository();
        private QuestionssRepository _question = new QuestionssRepository();
        private AnswersRepository _answer = new AnswersRepository();
        //Get Assessment Info
        //Get Questions in this assessment
        //Get Answers in each questions

        // GET api/values
        public string Get()
        {
            var userName = this.RequestContext.Principal.Identity.Name;
            return String.Format("Hello, {0}.", userName);
        }

        /// <summary>
        /// Get Active Assessments for signed in user.
        /// </summary>
        /// <returns></returns>
        [Route("GetActiveAssessments")]
        [HttpGet]
        public IHttpActionResult GetAssessments()
        {
            var results = _userAssessment.GetActiveUserAssessment(User.Identity.GetUserId());
            return Ok(results);
        }

        /// <summary>
        /// Get all question - find by assessmentId
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [Route("{id}/Questions")]
        [HttpGet]
        public IHttpActionResult GetQuestions(int id)
        {
            List<QuestionViewModel> results = new List<QuestionViewModel>();
            Random r = new Random();
            var allAssessments = _question.GetAllByAssessmentId(id).Shuffle(r);
            foreach (var item in allAssessments)
            {
                QuestionViewModel a = new QuestionViewModel();
                a.Id = item.Id;
                a.Name = item.Name;
                a.Position = item.Position;
                a.Answers = _answer.GetAll(item.Id).Select(n => new AnswerViewModel { Id = n.Id, Name = n.Name, Point = n.Point, Position = n.Position }).Shuffle(r).ToList();
                results.Add(a);
            }

            return Ok(results);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [Authorize(Roles = "Admin, Instructor")]
        [Route("{id}/ViewQuestion")]
        [HttpGet]
        public IHttpActionResult ViewQuestion(int id)
        {
            List<QuestionViewModel> results = new List<QuestionViewModel>();
            var allAssessments = _question.GetAllByAssessmentId(id);
            return Ok(allAssessments);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="answerCollection"></param>
        /// <returns></returns>
        [Route("SubmitAnswer")]
        [HttpPost]
        public IHttpActionResult SubmitAnswer(SubmitAnswerViewModel answerCollection)
        {
            if (answerCollection != null)
            {
                UserAssessments updateUserAssessment = _userAssessment.GetById(answerCollection.UserAssessmentId);
                if (updateUserAssessment == null)
                    return BadRequest(String.Format("There is no userAssessmentId({0}) in our system", answerCollection.UserAssessmentId));
                if (updateUserAssessment.IsFinished)
                    return Ok("This assessment already done!");

                foreach (var item in answerCollection.AnswerSelections)
                {
                    var evaluateAnswer = _answer.GetById(item.AnswerId);
                    if (evaluateAnswer.QuestionId == item.QuestionId)
                    {
                        _userAssessmentDetail.Add(new UserAssessmentDetails
                        {
                            Point = evaluateAnswer.Point,
                            QuestionId = item.QuestionId,
                            AnswerId = item.AnswerId,
                            UserAssessmentId = answerCollection.UserAssessmentId
                        });
                    }
                    else
                    {
                        return BadRequest(String.Format("This answerId({0}) has not for this questionId({1})", item.AnswerId, item.QuestionId));
                    }
                }

                var finalScore = _userAssessmentDetail.FinalizeScore(answerCollection.UserAssessmentId);

                updateUserAssessment.Id = answerCollection.UserAssessmentId;
                updateUserAssessment.Score = finalScore;
                updateUserAssessment.IsFinished = true;
                _userAssessment.Update(updateUserAssessment);
                return Ok("ส่งข้อสอบสำเร็จ");
            }

            return BadRequest("invalid json format for this URI");
        }
    }
}
