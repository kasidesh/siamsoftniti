﻿using Core.Entities;
using Core.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using PagedList;

namespace AssessmentApp.Controllers
{
    public class CourseSyllabusesController : Controller
    {
        private CourseSyllabusesRepository db = new CourseSyllabusesRepository();

        // GET: /CourseSyllabuses/
        public ActionResult Index(int? page)
        {
            int pageSize = 10;
            int pageNumber = (page ?? 1);
            var pageResult = db.GetAll().ToPagedList(pageNumber, pageSize);
            return View(pageResult);
        }

        // GET: /CourseSyllabuses/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            CourseSyllabuses worker = db.GetById(id.Value);
            if (worker == null)
            {
                return HttpNotFound();
            }
            return View(worker);
        }

        // GET: /CourseSyllabuses/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: /CourseSyllabuses/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "Id, Name, IsDeleted, CreatedDate, ModifiedDate")] CourseSyllabuses worker)
        {
            if (ModelState.IsValid)
            {
                db.Add(worker);
                return RedirectToAction("Index");
            }

            return View(worker);
        }

        // GET: /CourseSyllabuses/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            CourseSyllabuses worker = db.GetById(id.Value);
            if (worker == null)
            {
                return HttpNotFound();
            }
            return View(worker);
        }

        // POST: /CourseSyllabuses/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "Id, Name, IsDeleted, CreatedDate, ModifiedDate")] CourseSyllabuses worker)
        {
            if (ModelState.IsValid)
            {
                //db.Entry(worker).State = EntityState.Modified;
                //db.SaveChanges();
                db.Update(worker);
                return RedirectToAction("Index");
            }
            return View(worker);
        }

        // GET: /CourseSyllabuses/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            CourseSyllabuses worker = db.GetById(id.Value);
            if (worker == null)
            {
                return HttpNotFound();
            }
            return View(worker);
        }

        // POST: /CourseSyllabuses/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            //Department worker = db.GetById(id);
            db.Remove(id);
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}