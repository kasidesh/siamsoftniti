﻿using Core.Entities;
using Core.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using Microsoft.AspNet.Identity;
using Core.ViewModels;
using PagedList;

namespace AssessmentApp.Controllers
{
    public class SubjectsController : Controller
    {
        private SubjectsRepository _subject = new SubjectsRepository();
        private DepartmentsRepository _department = new DepartmentsRepository();
        private CourseSyllabusesRepository _courseSyllabus = new CourseSyllabusesRepository();

        // GET: /Scaffolding/
        public ActionResult Index(int? page)
        {
            int pageSize = 10;
            int pageNumber = (page ?? 1);
            var pageResult = _subject.GetAllLite().ToPagedList(pageNumber, pageSize);
            return View(pageResult);
        }

        // GET: /Scaffolding/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            SubjectViewModel worker = _subject.GetByIdExtend(id.Value);
            if (worker == null)
            {
                return HttpNotFound();
            }
            return View(worker);
        }

        // GET: /Scaffolding/Create
        public ActionResult Create()
        {
            //IEnumerable<SelectListItem> departments = _department.GetAll().Select(n => new SelectListItem { Text = n.Name, Value = n.Id.ToString() });
            //IEnumerable<SelectListItem> courseSyllabuses = _courseSyllabus.GetAll().Select(n => new SelectListItem { Text = n.Name, Value = n.Id.ToString() });
            //ViewBag.Departments = departments;
            //ViewBag.CourseSyllabuses = courseSyllabuses;

            PopulateDepartmentDropDownList();
            PopulateCourseSyllabusDropDownList();

            return View();
        }

        // POST: /Scaffolding/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "Id, Name, Code, DepartmentId, CourseSyllabusId, IsDeleted, CreatedDate, ModifiedDate")] Subjects worker)
        {
            if (ModelState.IsValid)
            {
                worker.UserId = User.Identity.GetUserId();
                _subject.Add(worker);
                return RedirectToAction("Index");
            }

            return View(worker);
        }

        // GET: /Scaffolding/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            //IEnumerable<SelectListItem> departments = _department.GetAll().Select(n => new SelectListItem { Text = n.Name, Value = n.Id.ToString() });
            //IEnumerable<SelectListItem> courseSyllabuses = _courseSyllabus.GetAll().Select(n => new SelectListItem { Text = n.Name, Value = n.Id.ToString() });
            //ViewBag.Departments = departments;
            //ViewBag.CourseSyllabuses = courseSyllabuses;

            Subjects worker = _subject.GetById(id.Value);
            
            if (worker == null)
            {
                return HttpNotFound();
            }
            PopulateDepartmentDropDownList(worker.DepartmentId);
            PopulateCourseSyllabusDropDownList(worker.CourseSyllabusId);
            return View(worker);
        }

        // POST: /Scaffolding/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "Id, Name, Code, DepartmentId, CourseSyllabusId, IsDeleted, CreatedDate, ModifiedDate")] Subjects worker)
        {
            if (ModelState.IsValid)
            {
                worker.UserId = User.Identity.GetUserId();
                //db.Entry(worker).State = EntityState.Modified;
                //db.SaveChanges();
                _subject.Update(worker);
                return RedirectToAction("Index");
            }
            return View(worker);
        }

        // GET: /Scaffolding/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            SubjectViewModel worker = _subject.GetByIdExtend(id.Value);
            if (worker == null)
            {
                return HttpNotFound();
            }
            return View(worker);
        }

        // POST: /Scaffolding/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            //Department worker = db.GetById(id);
            _subject.Remove(id);
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                _subject.Dispose();
            }
            base.Dispose(disposing);
        }

        private void PopulateDepartmentDropDownList(object selectedDepartment = null)
        {
            var departments = _department.GetAll();
            ViewBag.DepartmentId = new SelectList(departments, "Id", "Name", selectedDepartment);
        }

        private void PopulateCourseSyllabusDropDownList(object selectedCourseSyllabus = null)
        {
            var courseSyllabuses = _courseSyllabus.GetAll();
            ViewBag.CourseSyllabusId = new SelectList(courseSyllabuses, "Id", "Name", selectedCourseSyllabus);
        }
    }
}