﻿using AssessmentApp.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Microsoft.AspNet.Identity;
using System.Threading.Tasks;
using System.Web.Http;
using Microsoft.AspNet.Identity.Owin;
using Microsoft.Owin.Security;
using System.Web.Http;
using System.IO;
using CsvHelper;
using PagedList;

namespace AssessmentApp.Controllers
{
    public class UsersController : Controller
    {
        private ApplicationUserManager _userManager;
        private ApplicationRoleManager _roleManager;

        public ISecureDataFormat<AuthenticationTicket> AccessTokenFormat { get; private set; }
        public ApplicationUserManager UserManager
        {
            get
            {
                return _userManager ?? Request.GetOwinContext().GetUserManager<ApplicationUserManager>();
            }
            private set
            {
                _userManager = value;
            }
        }
        public ApplicationRoleManager RoleManager
        {
            get
            {
                return _roleManager ?? Request.GetOwinContext().GetUserManager<ApplicationRoleManager>();
            }
            private set
            {
                _roleManager = value;
            }
        }


        public UsersController()
        {
        }

        public UsersController(ApplicationUserManager userManager, ApplicationRoleManager roleManager,
            ISecureDataFormat<AuthenticationTicket> accessTokenFormat)
        {
            UserManager = userManager;
            RoleManager = roleManager;
            AccessTokenFormat = accessTokenFormat;
        }

        // GET: Users
        public ActionResult Admin(int? page)
        {
            int pageSize = 10;
            int pageNumber = (page ?? 1);
            var result = UserManager.Users.Where(n => n.Roles.Any(role => role.RoleId == "86dd1030-bd26-4552-b218-475307eafdde")).OrderBy(n => n.UserName).ToPagedList(pageNumber, pageSize);
            return View(result);
        }

        // GET: Instructor
        public ActionResult Instructor(int? page)
        {
            int pageSize = 10;
            int pageNumber = (page ?? 1);
            var result = UserManager.Users.Where(n => n.Roles.Any(role => role.RoleId == "86dd1030-bd26-4552-b218-475307ea11de")).OrderBy(n => n.UserName).ToPagedList(pageNumber, pageSize);
            return View(result);
        }

        // GET: Student
        public ActionResult Student(int? page)
        {
            int pageSize = 10;
            int pageNumber = (page ?? 1);
            var result = UserManager.Users.Where(n => n.Roles.Any(role => role.RoleId == "86dd1030-bd26-4552-b218-475307ea88de")).OrderBy(n => n.UserName).ToPagedList(pageNumber, pageSize);
            return View(result);
        }

        [System.Web.Http.HttpPost]
        public async Task<ActionResult> ImportStudents(HttpPostedFileBase file)
        {
            if (Request.Files["file"].ContentLength > 0)
            {
                string fileExtension =
                                     System.IO.Path.GetExtension(Request.Files["file"].FileName);

                if (fileExtension == ".csv")
                {
                    //read csv file
                    string fileLocation = Server.MapPath("~/Contents/") + Request.Files["file"].FileName;
                    if (System.IO.File.Exists(fileLocation))
                    {
                        System.IO.File.Delete(fileLocation);
                    }
                    Request.Files["file"].SaveAs(fileLocation);

                    using (TextReader textReader = System.IO.File.OpenText(fileLocation))
                    {
                        var csv = new CsvReader(textReader);
                        var records = csv.GetRecords<ImportUserViewModel>();

                        foreach (var item in records)
                        {
                            var user = new MyUser()
                            {
                                UserName = item.Username,
                                Email = item.Username
                                //MyUserInfos = new MyUser.MyUserInfo { FirstName = "", LastName = "" }
                            };
                            IdentityResult result = await UserManager.CreateAsync(user, item.Password);
                            if (!String.IsNullOrEmpty(user.Id))
                            {
                                IdentityResult assignRoleResult = await UserManager.AddToRoleAsync(user.Id, "Student");
                            }
                        }
                    }
                }
            }
            return RedirectToAction("Student");
        }

        [System.Web.Http.HttpPost]
        public async Task<ActionResult> ImportInstructors(HttpPostedFileBase file)
        {
            if (Request.Files["file"].ContentLength > 0)
            {
                string fileExtension =
                                     System.IO.Path.GetExtension(Request.Files["file"].FileName);

                if (fileExtension == ".csv")
                {
                    //read csv file
                    string fileLocation = Server.MapPath("~/Contents/") + Request.Files["file"].FileName;
                    if (System.IO.File.Exists(fileLocation))
                    {
                        System.IO.File.Delete(fileLocation);
                    }
                    Request.Files["file"].SaveAs(fileLocation);

                    using (TextReader textReader = System.IO.File.OpenText(fileLocation))
                    {
                        var csv = new CsvReader(textReader);
                        var records = csv.GetRecords<ImportUserViewModel>();

                        foreach (var item in records)
                        {
                            var user = new MyUser()
                            {
                                UserName = item.Username,
                                Email = item.Username
                                //MyUserInfos = new MyUser.MyUserInfo { FirstName = "", LastName = "" }
                            };
                            IdentityResult result = await UserManager.CreateAsync(user, item.Password);
                            if (!String.IsNullOrEmpty(user.Id))
                            {
                                IdentityResult assignRoleResult = await UserManager.AddToRoleAsync(user.Id, "Instructor");
                            }
                        }
                    }
                }
            }
            return RedirectToAction("Student");
        }

        public ActionResult Create()
        {
            var roles = RoleManager.Roles.ToList();

            List<SelectListItem> items = roles.Select(n => new SelectListItem { Text = n.Name, Value = n.Name }).ToList();


            ViewBag.Roles = items;
            //create selectedItem
            return View();
        }
    }
}