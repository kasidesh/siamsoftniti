﻿using Core.Entities;
using Core.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using Microsoft.AspNet.Identity;
using Core.ViewModels;
using PagedList;

namespace AssessmentApp.Controllers
{
    public class AssessmentsController : Controller
    {
        private AssessmentsRepository _assessment = new AssessmentsRepository();
        private SubjectsRepository _subject = new SubjectsRepository();
        private QuestionssRepository _question = new QuestionssRepository();
        private UserAssessmentsRepository _userAssessment = new UserAssessmentsRepository();

        // GET: /Assessments/
        public ActionResult Index()
        {
            return View(_assessment.GetAllLite().ToList());
        }

        // GET: /Assessments/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            AssessmentViewModel worker = _assessment.GetByIdLite(id.Value);
            if (worker == null)
            {
                return HttpNotFound();
            }
            return View(worker);
        }

        // GET: /Assessments/Create
        public ActionResult Create()
        {
            //IEnumerable<SelectListItem> subjects = _subject.GetAll().Select(n => new SelectListItem { Text = n.Name, Value = n.Id.ToString() });
            //ViewBag.Subjects = subjects;

            PopulateSubjectDropDownList();
            return View();
        }

        // POST: /Assessments/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "Id, SubjectId, Name, Duration, AmountOfQuestion, ExplainationText, PassingScore, FullScore, SchoolYear, IsDeleted, CreatedDate, ModifiedDate")] Assessments worker)
        {
            if (ModelState.IsValid)
            {
                worker.UserId = User.Identity.GetUserId();
                _assessment.Add(worker);
                return RedirectToAction("Index");
            }

            return View(worker);
        }

        // GET: /Assessments/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            //IEnumerable<SelectListItem> subjects = _subject.GetAll().Select(n => new SelectListItem { Text = n.Name, Value = n.Id.ToString() });
            //ViewBag.Subjects = subjects;

            AssessmentViewModel worker = _assessment.GetByIdLite(id.Value);
            PopulateSubjectDropDownList(worker.SubjectId);
            if (worker == null)
            {
                return HttpNotFound();
            }
            return View(worker);
        }

        // POST: /Assessments/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "Id, SubjectId, Name, Duration, AmountOfQuestion, ExplainationText, PassingScore, FullScore, SchoolYear, IsDeleted, CreatedDate, ModifiedDate")] Assessments worker)
        {
            if (ModelState.IsValid)
            {
                //db.Entry(worker).State = EntityState.Modified;
                //db.SaveChanges();
                worker.UserId = User.Identity.GetUserId();
                _assessment.Update(worker);
                return RedirectToAction("Index");
            }
            return View(worker);
        }

        // GET: /Assessments/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            AssessmentViewModel worker = _assessment.GetByIdLite(id.Value);
            if (worker == null)
            {
                return HttpNotFound();
            }
            return View(worker);
        }

        // POST: /Assessments/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            //Department worker = db.GetById(id);
            _assessment.Remove(id);
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                _assessment.Dispose();
            }
            base.Dispose(disposing);
        }

        // GET: /Assessments/ViewQuestions/5
        public ActionResult ViewQuestions(int id, int? page)
        {
            int pageSize = 10;
            int pageNumber = (page ?? 1);
            var pageResult = _question.GetAllByAssessmentId(id).ToPagedList(pageNumber, pageSize);

            ViewBag.AssessmentId = id;
            return View(pageResult);
        }

        public ActionResult ViewAssessmentResult(int id)
        {
            var userAssessments = _userAssessment.GetUserAssessmentByAssessmentId(id);
            return View(userAssessments);
        }

        public ActionResult ViewAssessmentResultDetails(int id)
        {
            return View();
        }

        private void PopulateSubjectDropDownList(object selectedSubject = null)
        {
            var subjects = _subject.GetAll();
            ViewBag.SubjectId = new SelectList(subjects, "Id", "Name", selectedSubject);
        }
    }
}